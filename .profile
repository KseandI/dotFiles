. ${HOME}/.local/user-dirs.dirs

export PATH="${XDG_EXECUTABLE_HOME}/virt_bin:${XDG_EXECUTABLE_HOME}:/usr/sbin:$PATH"

export XKB_DEFAULT_OPTIONS="grp:lctrl_lwin_toggle,grp_led:caps,ctrl:nocaps,lv3:ralt_switch"
export XKB_DEFAULT_LAYOUT="us,ru"
export XKB_DEFAULT_VARIANT="dvp,ruu"
export XKB_DEFAULT_MODEL="pc105"
#export $(dbus-launch)

export _JAVA_AWT_WM_NONREPARENTING=1
export QT_QPA_PLATFORMTHEME=qt5ct
export LSP_USE_PLISTS=true
export LIBVA_DRIVER_NAME=nvidia
export MOZ_ENABLE_WAYLAND=1
export MOZ_DISABLE_RDD_SANDBOX=1
export MOZ_X11_EGL=1
export MOZ_NO_REMOTE=1
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export DRI_PRIME=1
[ -z ${XDG_RUNTIME_DIR} ] && export XDG_RUNTIME_DIR=/tmp/runtime-${USER}

export EDITOR=nvim
export VISUAL=emacs
export PAGER=less

export LOCALE_ARCHIVE=/usr/lib/locale/locale-archive
export LC_ALL=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_COLLATE=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8


# non-XDG dirs
export NETHACKOPTIONS="${XDG_CONFIG_HOME}/nethack/config"
export INPUTRC="${XDG_CONFIG_HOME}/readline/inputrc"
export MPD_HOST="${XDG_RUNTIME_DIR}/mpd/socket"
export GRIM_DEFAULT_DIR="${XDG_PICTURES_DIR}/screenshots"
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export PYTHONUSERBASE="${XDG_DATA_HOME}/python"
export PYTHONHISTFILE="${PYTHONUSERBASE}/history"
export PYTHONPYCACHEPREFIX="${XDG_CACHE_HOME}/python"
export PATH="$PATH:${PYTHONUSERBASE}/bin"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="${XDG_CONFIG_HOME}/java"
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export GTK_RC_FILES="${XDG_CONFIG_HOME}/gtk-1.0/gtkrc"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"
export GHCUP_USE_XDG_DIRS=true
export NUGET_PACKAGES="${XDG_CACHE_HOME}/NuGetPackages"
export GIT_CONFIG_GLOBAL="${XDG_CONFIG_HOME}/git/config"
export RLWRAP_HOME="${XDG_DATA_HOME}/rlwrap"
export DOCKER_CONFIG="${XDG_CONFIG_HOME}/docker"
export ANSIBLE_HOME="${XDG_CONFIG_HOME}/ansible"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME}/ansible.cfg"
export ANSIBLE_GALAXY_CACHE_DIR="${XDG_CACHE_HOME}/ansible/galaxy_cache"
export ENV="${XDG_CONFIG_HOME}/shell/config"
