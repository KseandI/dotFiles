#!/bin/sh

dotfiles_directory=${HOME}/.local/share/dotfiles
user_dirs_file=${HOME}/.local/user-dirs.dirs

git_repository="https://gitlab.com"
git_user="c4llv07e"
git_dotfiles_repo="dotFiles.git"
git_scripts_repo="scripts.git"

required_commands="git printf mkdir cat"

git_dotfiles_repo_url=$(printf "%s/%s/%s" ${git_repository} ${git_user} ${git_dotfiles_repo})
git_scripts_repo_url=$(printf "%s/%s/%s" ${git_repository} ${git_user} ${git_scripts_repo})

requirements_not_satisfied=
for com in $required_commands; do
    if ! command -v $com > /dev/null 2> /dev/null; then
        printf "Error, can't find \`%s\` command\n" ${com}
        requirements_not_satisfied=1
    fi
done

if [ $requirements_not_satisfied ]; then
    printf "Requirements are not satisfied, exiting\n"
    exit
fi

# pull DotFilesConfig
git clone --bare ${git_dotfiles_repo_url} ${dotfiles_directory}
git --git-dir=${dotfiles_directory} --work-tree=$HOME checkout --force
git --git-dir=${dotfiles_directory} --work-tree=$HOME config \
  core.excludesFile .local/config/dotfiles/exclude

. ${user_dirs_file}

# make directories
mkdir -p ${XDG_STATE_HOME} ${XDG_EXECUTABLE_HOME} ${XDG_SOURCE_DIR} \
  ${XDG_RUNTIME_DIR}

chmod 700 ${XDG_RUNTIME_DIR}

# installing scripts
scripts_directory=${XDG_SOURCE_DIR}/scripts
git clone ${git_scripts_repo_url} ${scripts_directory}
${scripts_directory}/INSTALL.sh
