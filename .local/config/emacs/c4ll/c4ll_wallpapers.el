
(defvar c4ll/current_wallpaper_path
  "~/home/media/image/wallpaper")

(defvar c4ll/wallpapers_directory
  "~/home/media/image/wallpapers")

(defun c4ll/get_random_file_in_directory (directory)
  (let ((files (cdr (cdr (directory-files directory)))))
    (nth (% (abs (random)) (length files)) files)))

(defun c4ll/select_wallpaper (image_path)
  (if (file-exists-p c4ll/current_wallpaper_path)
      (delete-file c4ll/current_wallpaper_path))
  (make-symbolic-link image_path c4ll/current_wallpaper_path))

(defun c4ll/random_wallpaper ()
  (interactive)
  (c4ll/select_wallpaper
   (expand-file-name
    (c4ll/get_random_file_in_directory c4ll/wallpapers_directory)
    c4ll/wallpapers_directory)))

(provide 'c4ll_wallpapers)
