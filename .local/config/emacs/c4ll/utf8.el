
(make-variable-buffer-local
 (defvar utf8-mode nil
   "Use UTF8 font in buffer."))
(defvar utf8-default-font "Liberation Mono 11"
  "Default font.")

(add-to-list 'minor-mode-alist '(utf8-mode " utf8"))

(defun utf8-mode (&optional ARG)
  (interactive)
  (if ARG
      (setq mode (> ARG 0))
    (setq mode (not utf8-mode)))
  (setq utf8-mode mode)
  (if utf8-mode
        (set-frame-font "UTF8 48" nil t)
    (set-frame-font utf8-default-font nil t)))

(provide 'utf8)
