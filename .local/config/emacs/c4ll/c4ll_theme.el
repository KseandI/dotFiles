
;(setq modus-themes-custom-auto-reload nil
;      modus-themes-disable-other-themes t)

(setq modus-vivendi-palette-overrides
      '(
        (bg-main "#081508")
        (bg-dim  "#041004")
        (bg-alt  "#105010")

        (border bg-dim)

        (bg-line-number-active "#004000")

        (bg-mode-line-active bg-alt)
        (bg-mode-line-inactive "#102810")

        ;; (comment red-faint)
        ;; (constant green-faint)
        ;; (docstring blue-faint)
        ;; (fnname green-intense)
        ;; (keyword magenta-cooler)
        ;; (preprocessor red-warmer)
        ;; (string blue)
        ;; (type cyan-cooler)
        ;; (variable cyan-warmer)

        ;; (rainbow-0 magenta-intense)
        ;; (rainbow-1 red-warmer)
        ;; (rainbow-2 yellow-intense)
        ;; (rainbow-3 magenta-cooler)
        ;; (rainbow-4 green-intense)
        ;; (rainbow-5 blue-warmer)
        ;; (rainbow-6 magenta-warmer)
        ;; (rainbow-7 fg-main)
        ;; (rainbow-8 cyan-intense)

        (bg-region "#211950")
        ))

(setq modus-themes-common-palette-overrides
      '(
        (border-mode-line-active unspecified)
        (border-mode-line-inactive unspecified)
        (fg-region unspecified)
        ))

(custom-set-faces
 '(mc/cursor-bar-face ((t (:background "#eeeeee" :height 3)))))
(load-theme 'modus-vivendi t)

(provide 'c4ll_theme)
