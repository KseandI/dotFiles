;;; init.el --- c4llv07e's startup script -*- coding: utf-8; lexical-binding: t; -*-
(setq lexical-binding t)
(defmacro comment (&rest message))

(defvar native-comp-deferred-compilation-deny-list nil)

(require 'utf8)

(require 'whitespace)

(setq custom-file (locate-user-emacs-file "custom-set-vars.el"))
(load custom-file 'noerror 'nomessage)

(defun project-compile-arg (COMMAND)
  "Run `compile' in the project root."
  (declare (interactive-only compile))
  (interactive)
  (let ((default-directory (project-root (project-current t)))
        (compilation-buffer-name-function
         (or project-compilation-buffer-name-function
             compilation-buffer-name-function)))
    (compile COMMAND)))

(defun save-if-file ()
  (if buffer-file-name
      (save-buffer)
    nil))

(defun c4/generate-name ()
  (interactive)
  (let ((result ""))
    (dotimes (i #x20)
      (let ((number (random 36)))
        (if (< number 10)
            (setq result (format "%s%s" result (number-to-string number)))
          (setq result (format "%s%c" result (+ (- number 10) ?a))))))
    result))

(defun save-on-focus-lost ()
  (interactive)
  (if (not (member 'save-if-file focus-out-hook))
      (add-hook 'focus-out-hook 'save-if-file nil t)
    (remove-hook 'focus-out-hook 'save-if-file t)))

(defun set-transparency (value)
  (interactive "nTransparency value: ")
  (set-frame-parameter nil 'alpha-background value))

(defun global-save-on-focus-lost ()
  (interactive)
  (if (not (member 'save-if-file focus-out-hook))
      (add-hook 'focus-out-hook 'save-if-file)
    (remove-hook 'focus-out-hook 'save-if-file)))

(defun run-project ()
  (project-compile-arg "make -k run"))

(defun run-project-on-save ()
  (interactive)
  (if (not (member 'run-project after-save-hook))
      (add-hook 'after-save-hook 'run-project nil t)
    (remove-hook 'after-save-hook 'run-project t)))

(defun display-startup-echo-area-message ()
  "Do nothing.")

(defun disable-line-numbers ()
  "Disable line numbers in current buffer."
  (display-line-numbers-mode 0))

(defun back-other-window ()
  "Go one window back."
  (interactive)
  (other-window -1))

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

(setq-default native-comp-async-report-warnings-errors 'silent
              truncate-string-ellipsis "…"
              require-final-newline t
              sentence-end-double-space nil
              fill-column 80
              scroll-margin 2
              tab-width 1
              display-line-numbers 'relative
              cursor-type '(bar . 2)
              cursor-in-non-selected-windows '(hbar . 2)
              indent-tabs-mode nil
              )

(setq create-lockfiles nil
      make-backup-files nil
      auto-save-list-file-name nil
      auto-save-default nil
      display-time-day-and-date t
      display-time-24hr-format t
      display-time-format "%Y-%m-%dT%T"
      display-time-interval 1
      display-line-numbers 'relative
      xwidget-webkit-cookie-file (locate-user-emacs-file "webkit-cookie")
      history-length 100
      package-install-upgrade-built-in t
      inhibit-startup-message t
      inhibit-startup-echo-area-message t
      initial-scratch-message nil
      shift-select-mode nil
      copy-region-blink-delay 0 ; emacs, just why?
      use-file-dialog nil
      compile-command "./build.sh"
      compilation-scroll-output t
      use-dialog-box nil
      inhibit-default-init t
      ns-pop-up-frames nil
      line-move-visual nil
      pop-up-windows t
      straight-process-buffer " "
      ido-record-commands nil
      ido-enable-last-directory-history nil
      tab-width 1
      c-default-style "k&r"
      c-basic-offset 1
      fill-column 75
      next-screen-context-lines 8
      list-matching-lines-default-context-lines 0
      org-blank-before-new-entry '((heading . t) (plain-list-item . t))
      grep-find-command '("grep --color=auto -nHRI \"\"" . 26)
      whitespace-style '(face tabs spaces trailing space-before-tab
                              indentation space-after-tab space-mark tab-mark)
      )

(set-face-foreground 'whitespace-hspace "#00a080")
(set-face-foreground 'whitespace-indentation "#00a080")
(set-face-foreground 'whitespace-newline "#00a080")
(set-face-foreground 'whitespace-space "#00a080")
(set-face-foreground 'whitespace-tab "#00a080")

(fset 'yes-or-no-p 'y-or-n-p)
(defalias 'yes-or-not-p 'y-or-n-p)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'minibuffer-history 'history-length 50)
(put 'evil-ex-history 'history-length 50)
(put 'kill-ring 'history-length 25)

(display-time-mode)
(column-number-mode)
(line-number-mode)
(global-subword-mode)
(transient-mark-mode 1)
(electric-indent-mode)
(fido-vertical-mode)
(global-whitespace-mode)

(blink-cursor-mode 0)
(set-frame-font "Liberation Mono 10" nil t)
(set-cursor-color "#00ffff")
(add-to-list 'default-frame-alist '(alpha-background . 90))
(require 'c4ll_theme)

(add-hook 'eshell-mode-hook 'disable-line-numbers)
(add-hook 'shell-mode-hook 'disable-line-numbers)
(add-hook 'ses-mode-hook #'(lambda ()
                             (setq display-line-numbers t)))

(setq parens-require-spaces nil)
(add-hook 'emacs-lisp-mode-hook #'(lambda ()
                                    (setq-local parens-require-spaces t)))
(add-hook 'lisp-mode-hook #'(lambda ()
                              (setq-local parens-require-spaces t)))

(global-set-key (kbd "M-F") 'forward-to-word)
(global-set-key (kbd "M-B") 'backward-to-word)
(global-set-key (kbd "M-O") 'back-other-window)
(global-set-key (kbd "C-x O") 'back-other-window)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-Z") 'zap-up-to-char)
(add-hook 'org-mode-hook
          #'(lambda ()
              (define-key org-mode-map (kbd "C-c t") 'org-timestamp)))

;; remap window resize keys
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)

(global-set-key (kbd "M-<left>")
                (lambda () (interactive) (shrink-window-horizontally 4)))
(global-set-key (kbd "M-<right>")
                (lambda () (interactive) (enlarge-window-horizontally 4)))
(global-set-key (kbd "M-<down>")
                (lambda () (interactive) (shrink-window 4)))
(global-set-key (kbd "M-<up>")
                (lambda () (interactive) (enlarge-window 4)))

(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-c C-z"))
(global-unset-key (kbd "C-x C-n"))

(defun my-indent-setup ()
  (c-set-offset 'arglist-intro '+))
(add-hook 'c-mode-hook 'my-indent-setup)

(defun c4ll/haskell-config ()
  (eldoc-mode -1))
(if (boundp 'haskell-mode-hook)
    (add-hook 'haskell-mode-hook 'c4ll/haskell-config))

(defun c4ll/init-package-manager ()
  (eval-and-compile
    (defvar bootstrap-version)
    (setq straight-repository-branch "develop") ; TEMPORARY issue with straight, see https://jeffkreeftmeijer.com/emacs-straight-use-package/
    (let ((bootstrap-file
           (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
          (bootstrap-version 6))
      (unless (file-exists-p bootstrap-file)
        (with-current-buffer
            (url-retrieve-synchronously
             "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
             'silent 'inhibit-cookies)
          (goto-char (point-max))
          (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage))

    (straight-use-package '(use-package :type built-in))
    (require 'bind-key)
    (setq straight-use-package-by-default t)))

(add-to-list 'auto-mode-alist '("\\.xaml\\'" . xml-mode))

(defun c4ll/install-packages ()
  (c4ll/init-package-manager)

  (use-package rainbow-delimiters
    :hook (prog-mode . rainbow-delimiters-mode))

  (use-package haskell-mode
    :hook (haskell-mode . interactive-haskell-mode))

  (use-package multiple-cursors
    :bind (("C->" . mc/mark-next-like-this)
           ("C-<" . mc/mark-previous-like-this)))

  (use-package proof-general
    :config (setq proof-three-window-mode-policy 'hybrid))

  (use-package editorconfig
    :config (editorconfig-mode 1))

  (straight-use-package 'org)
  (use-package org-roam
    :custom
    (org-roam-directory "~/home/data/notes")
    (org-agenda-files `("~/home/data/notes"))
    (org-goto-auto-isearch nil)
    (org-startup-folded t)
    :bind (("C-c n l" . org-roam-buffer-toggle)
           ("C-c n f" . org-roam-node-find)
           ("C-c n i" . org-roam-node-insert))
    :config
    (org-roam-setup))
  (comment I don't use it right now
   (use-package company
     :bind (("M-n" . company-select-next-or-abort)
            ("M-p" . company-select-previous-or-abort)
            ("M-j" . company-complete-selection))
     :custom
     (company-idle-delay 0)
     (company-minimum-prefix-length 0)
     (company-format-margin-function nil)
     :config
     (setcdr company-active-map nil))
   (straight-use-package 'xref)
   (straight-use-package 'eldoc)
   (straight-use-package 'project)
   (straight-use-package 'flymake)
   (use-package eglot
     :config
     (add-to-list 'eglot-server-programs
                  '(csharp-mode . ("csharp-ls"))))
   )
  (defalias 'yaml-mode 'yaml-ts-mode)

  (use-package markdown-mode
    :bind (("C-<return>" . markdown-follow-link-at-point)))

  (straight-use-package 'tsc)
  (straight-use-package 'tree-sitter)
  (straight-use-package 'tree-sitter-langs)

  (require 'tree-sitter)
  (require 'tree-sitter-langs)
  (require 'treesit)

  (use-package glsl-mode))
(c4ll/install-packages)

(add-hook 'prog-mode-hook 'global-eldoc-mode)
(add-hook 'prog-mode-hook 'global-ede-mode)
(add-hook 'yaml-ts-mode-hook #'(lambda ()
                                 (setq-local tab-width 2)))

(setq auto-mode-alist
      (delete-dups
       (append
        '(("\\.go\\'" . go-ts-mode)
          ("\\.ya?ml\\'" . yaml-ts-mode)
          ("Dockerfile\\'" . dockerfile-ts-mode)
          ("\\.mod\\'" . go-mod-ts-mode))
        auto-mode-alist)))

(setq inferior-lisp-program "sbcl")
(setq dired-dwim-target t)
(setq tramp-terminal-type "dumb")

(defun append-shell ()
  (let ((extension (file-name-extension buffer-file-name))
        (exists (file-exists-p buffer-file-name)))
    (cond ((equal extension "sh")
           (unless exists
             (insert "#!/bin/sh\n\nset -xe\n\n")))
          ((equal extension "py")
           (unless exists
             (insert "#!/bin/env python3\n"))))))

(add-hook 'find-file-hook #'append-shell)

(defun kill-buffer--possibly-save (buffer)
  (y-or-n-p "buffer unsaved; kill it?"))

(defun eval-print-last-sexp (&optional eval-last-sexp-arg-internal)
  (interactive "P")
  (let ((standard-output (current-buffer)))
    (terpri)
    (insert ";; ")
    (eval-last-sexp t)
    (terpri)))

(defun icomplete-fido-backward-updir ()
  "Delete char before or go up directory, like `ido-mode'."
  (interactive)
  (call-interactively 'backward-delete-char))

(defun icomplete-fido-ret ()
  "Exit minibuffer or enter directory, like `ido-mode'."
  (interactive)
  (call-interactively 'icomplete-fido-exit))

(define-key icomplete-minibuffer-map (kbd "<tab>") 'icomplete-force-complete)
